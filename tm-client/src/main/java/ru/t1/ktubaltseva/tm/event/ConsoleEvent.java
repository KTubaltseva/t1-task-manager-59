package ru.t1.ktubaltseva.tm.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@AllArgsConstructor
public class ConsoleEvent {

    @NotNull
    private final String name;

}
