package ru.t1.ktubaltseva.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataSaveBase64Request;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataSaveBase64Listener extends AbstractDataListener {

    @NotNull
    private final String NAME = "data-save-base64";

    @NotNull
    private final String DESC = "Save data to base64 file.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@dataSaveBase64Listener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SAVE BASE64 DATA]");
        @NotNull final DataSaveBase64Request request = new DataSaveBase64Request(getToken());
        getDomainEndpoint().saveDataBase64(request);
    }

}
