package ru.t1.ktubaltseva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.listener.AbstractListener;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    private final String NAME = "help";

    @NotNull
    private final String DESC = "Display list of commands.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        @NotNull String ARGUMENT = "-h";
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {

        System.out.println("[HELP]");
        for (@NotNull final AbstractListener listener : listeners) System.out.println(listener);


    }

}
