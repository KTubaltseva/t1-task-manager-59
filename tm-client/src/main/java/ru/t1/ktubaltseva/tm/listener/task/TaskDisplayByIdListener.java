package ru.t1.ktubaltseva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.dto.request.task.TaskDisplayByIdRequest;
import ru.t1.ktubaltseva.tm.dto.response.task.TaskDisplayByIdResponse;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

@Component
public final class TaskDisplayByIdListener extends AbstractTaskListener {

    @NotNull
    private final String NAME = "task-display-by-id";

    @NotNull
    private final String DESC = "Display task by Id.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@taskDisplayByIdListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DISPLAY PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskDisplayByIdRequest request = new TaskDisplayByIdRequest(getToken(), id);
        @NotNull final TaskDisplayByIdResponse response = getTaskEndpoint().getTaskById(request);
        @Nullable final TaskDTO task = response.getTask();
        displayTask(task);
    }

}
