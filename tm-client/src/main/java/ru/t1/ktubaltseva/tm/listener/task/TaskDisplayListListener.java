package ru.t1.ktubaltseva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.dto.request.task.TaskDisplayListRequest;
import ru.t1.ktubaltseva.tm.dto.response.task.TaskDisplayListResponse;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskDisplayListListener extends AbstractTaskListener {

    @NotNull
    private final String NAME = "task-list";

    @NotNull
    private final String DESC = "Display task list.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@taskDisplayListListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DISPLAY TASKS]");
        System.out.println("[ENTER SORT]:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskDisplayListRequest request = new TaskDisplayListRequest(getToken(), sort);
        @NotNull final TaskDisplayListResponse response = getTaskEndpoint().getAllTasks(request);
        @Nullable final List<TaskDTO> tasks = response.getTasks();
        renderTasks(tasks);
    }

}
