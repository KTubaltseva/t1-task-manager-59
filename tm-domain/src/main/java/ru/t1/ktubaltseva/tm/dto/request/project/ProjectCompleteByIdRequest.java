package ru.t1.ktubaltseva.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCompleteByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectCompleteByIdRequest(@Nullable final String token) {
        super(token);
    }

    public ProjectCompleteByIdRequest(@Nullable final String token, @Nullable final String id) {
        super(token);
        this.id = id;
    }

}
