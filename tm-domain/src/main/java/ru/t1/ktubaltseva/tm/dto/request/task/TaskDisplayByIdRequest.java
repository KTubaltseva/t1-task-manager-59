package ru.t1.ktubaltseva.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskDisplayByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskDisplayByIdRequest(@Nullable final String token) {
        super(token);
    }

    public TaskDisplayByIdRequest(@Nullable final String token, @Nullable final String id) {
        super(token);
        this.id = id;
    }

}
