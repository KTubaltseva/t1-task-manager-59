package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;


public interface ILoggerService {

    void log(@NotNull String text);

}
