package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.listener.EntityListener;

import javax.jms.*;

import static ru.t1.ktubaltseva.tm.constant.LoggerConstant.QUEUE;

@Service
public final class ReceiverService {

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    public void receive(@NotNull final EntityListener entityListener) throws JMSException {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();

        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);

        System.out.println("** LOGGER STARTED **");
    }
}
