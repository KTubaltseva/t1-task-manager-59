package ru.t1.ktubaltseva.tm.api.service;

import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.Domain;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.data.LoadDataException;
import ru.t1.ktubaltseva.tm.exception.data.SaveDataException;

public interface IDomainService {

    @NotNull
    Domain getDomain() throws AbstractException;

    void setDomain(@Nullable Domain domain) throws AbstractException;

    void loadDataBackup() throws LoadDataException;

    void loadDataBase64() throws LoadDataException;

    void loadDataBinary() throws LoadDataException;

    void loadDataJsonFasterXml() throws LoadDataException;

    void loadDataJsonJaxB() throws LoadDataException;

    void loadDataXmlFasterXml() throws LoadDataException;

    void loadDataXmlJaxB() throws LoadDataException;

    void loadDataYamlFasterXml() throws LoadDataException;

    void saveDataBackup() throws SaveDataException;

    void saveDataBase64() throws SaveDataException;

    void saveDataBinary() throws SaveDataException;

    void saveDataJsonFasterXml() throws SaveDataException;

    void saveDataJsonJaxB() throws SaveDataException;

    void saveDataXmlFasterXml() throws SaveDataException;

    void saveDataXmlJaxB() throws SaveDataException;

    void saveDataYamlFasterXml() throws SaveDataException;

    void initScheme() throws LiquibaseException;

    void dropScheme() throws DatabaseException;

}

