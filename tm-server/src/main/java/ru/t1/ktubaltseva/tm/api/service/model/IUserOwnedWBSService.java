package ru.t1.ktubaltseva.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModelWBS;

import java.util.List;

public interface IUserOwnedWBSService<M extends AbstractUserOwnedModelWBS> extends IUserOwnedService<M> {

    @NotNull
    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException, InstantiationException, IllegalAccessException;

    @NotNull
    M create(
            @Nullable String userId,
            @Nullable String name
    ) throws AbstractException, InstantiationException, IllegalAccessException;

    @NotNull
    List<M> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    ) throws AbstractException;

    @NotNull
    M changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws AbstractException;

}
