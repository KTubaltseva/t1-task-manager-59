package ru.t1.ktubaltseva.tm.repository.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.api.repository.model.IRepository;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Getter
@Repository
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected abstract Class<M> getClazz();

    @NotNull
    protected abstract String getSortColumnName(@NotNull final Comparator comparator);

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear() {
        @Nullable final List<M> models = findAll();
        for (@NotNull final M model : models) {
            remove(model);
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName();
        return entityManager.createQuery(jpql, getClazz()).getResultList();
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName() + " ORDER BY " + getSortColumnName(comparator);
        return entityManager.createQuery(jpql, getClazz()).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return entityManager.find(getClazz(), id);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(1) FROM " + getClazz().getSimpleName();
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(entityManager.merge(model));
    }

    @Override
    public void removeById(@NotNull final String id) {
        Optional<M> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Nullable
    @Override
    public M update(@NotNull final M model) {
        return entityManager.merge(model);
    }

}
