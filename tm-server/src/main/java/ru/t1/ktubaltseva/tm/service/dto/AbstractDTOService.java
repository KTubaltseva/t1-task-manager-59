package ru.t1.ktubaltseva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.repository.dto.IDTORepository;
import ru.t1.ktubaltseva.tm.api.service.dto.IDTOService;
import ru.t1.ktubaltseva.tm.dto.model.AbstractModelDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.data.SqlDataException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;

import java.util.*;

@Service
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>> implements IDTOService<M> {

    @NotNull
    @Autowired
    private R repository;

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final M model) throws EntityNotFoundException, SqlDataException {
        if (model == null) throw new EntityNotFoundException();
        return repository.add(model);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(@Nullable final Collection<M> models) throws SqlDataException {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<M> resultModels = new ArrayList<>();
        for (@NotNull final M model : models) {
            @NotNull final M resultModel = repository.add(model);
            resultModels.add(resultModel);
        }
        return resultModels;
    }

    @Override
    @Transactional
    public void clear() throws AbstractException {
        repository.clear();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        try {
            findOneById(id);
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public List<M> findAll() throws AbstractException {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws AbstractException {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M resultModel = repository.findOneById(id);
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Override
    public int getSize() throws AbstractException {
        return repository.getSize();
    }

    @Override
    @Transactional
    public void remove(@Nullable final M model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        repository.remove(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new EntityNotFoundException();
        repository.removeById(id);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        clear();
        return add(models);
    }

    @NotNull
    @Override
    @Transactional
    public M update(
            @Nullable final M model
    ) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        @Nullable M resultModel = repository.update(model);
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

}
