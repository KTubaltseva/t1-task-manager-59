package ru.t1.ktubaltseva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;

@Service
public class ProjectDTOService extends AbstractUserOwnedWBSDTOService<ProjectDTO, IProjectDTORepository> implements IProjectDTOService {

    @NotNull
    @Autowired
    private IProjectDTORepository repository;

}
