package ru.t1.ktubaltseva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.ktubaltseva.tm.api.service.dto.ISessionDTOService;
import ru.t1.ktubaltseva.tm.dto.model.SessionDTO;

@Service
public class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, ISessionDTORepository> implements ISessionDTOService {

    @NotNull
    @Autowired
    private ISessionDTORepository repository;

}
