package ru.t1.ktubaltseva.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.repository.model.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectService;
import ru.t1.ktubaltseva.tm.model.Project;


@Service
public final class ProjectService extends AbstractUserOwnedWBSService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

}
