package ru.t1.ktubaltseva.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectService;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectTaskService;
import ru.t1.ktubaltseva.tm.api.service.model.ITaskService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.TaskIdEmptyException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;


@Service
public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Override
    @Transactional
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Project project;
        try {
            project = projectService.findOneById(userId, projectId);
        } catch (@NotNull final EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        @Nullable Task resultTask;
        try {
            resultTask = taskService.findOneById(userId, taskId);
        } catch (@NotNull final EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        resultTask.setProject(project);
        resultTask = taskService.update(userId, resultTask);
        return resultTask;
    }

    @NotNull
    @Override
    @Transactional
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        try {
            projectService.findOneById(userId, projectId);
        } catch (@NotNull final EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        @Nullable Task resultTask;
        try {
            resultTask = taskService.findOneById(userId, taskId);
        } catch (@NotNull final EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        resultTask.setProject(null);
        resultTask = taskService.update(userId, resultTask);
        return resultTask;
    }

    @Override
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        try {
            projectService.findOneById(userId, projectId);
        } catch (@NotNull final EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        taskService.removeAllByProjectId(userId, projectId);
        projectService.removeById(userId, projectId);
    }

    @Override
    @Transactional
    public void clearProjects(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @NotNull final List<Project> projects = projectService.findAll(userId);
        for (@NotNull final Project project : projects) {
            taskService.removeAllByProjectId(userId, project.getId());
        }
        projectService.clear(userId);
    }

}