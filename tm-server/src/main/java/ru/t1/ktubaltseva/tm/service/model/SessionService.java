package ru.t1.ktubaltseva.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.repository.model.ISessionRepository;
import ru.t1.ktubaltseva.tm.api.service.model.ISessionService;
import ru.t1.ktubaltseva.tm.model.Session;


@Service
public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository repository;

}
