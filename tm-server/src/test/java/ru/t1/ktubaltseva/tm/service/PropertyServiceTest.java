package ru.t1.ktubaltseva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest extends AbstractServiceTest {

    @NotNull
    private static IPropertyService service;

    @NotNull
    private static IPropertyService getService() {
        return context.getBean(IPropertyService.class);
    }

    @BeforeClass
    @SneakyThrows
    public static void beforeClazz() {
        service = getService();
    }

    @Test
    @Ignore
    public void getApplicationVersion() {
        Assert.assertNotNull(service.getApplicationVersion());
    }

    @Test
    @Ignore
    public void getAuthorEmail() {
        Assert.assertNotNull(service.getAuthorEmail());
    }

    @Test
    @Ignore
    public void getAuthorName() {
        Assert.assertNotNull(service.getAuthorName());
    }

    @Test
    @Ignore
    public void getGitBranch() {
        Assert.assertNotNull(service.getGitBranch());
    }

    @Test
    @Ignore
    public void getGitCommitId() {
        Assert.assertNotNull(service.getGitCommitId());
    }

    @Test
    @Ignore
    public void getGitCommitterName() {
        Assert.assertNotNull(service.getGitCommitterName());
    }

    @Test
    @Ignore
    public void getGitCommitterEmail() {
        Assert.assertNotNull(service.getGitCommitterEmail());
    }

    @Test
    @Ignore
    public void getGitCommitMessage() {
        Assert.assertNotNull(service.getGitCommitMessage());
    }

    @Test
    @Ignore
    public void getGitCommitTime() {
        Assert.assertNotNull(service.getGitCommitTime());
    }

    @Test
    public void getServerPort() {
        Assert.assertNotNull(service.getServerPort());
    }

    @Test
    public void getServerHost() {
        Assert.assertNotNull(service.getServerHost());
    }

    @Test
    public void getSessionTimeout() {
        Assert.assertNotNull(service.getSessionTimeout());
    }

    @Test
    public void getSessionKey() {
        Assert.assertNotNull(service.getSessionKey());
    }

}
